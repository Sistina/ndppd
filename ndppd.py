#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Pass your ifname[s] as parameter[s]"""

import subprocess
import time
import os
import sys

size = 0
args = sys.argv[1:]
interfaces = dict(zip(args, [0 for i in args]))


def proxy_add(ip, ifname):
    subprocess.call("ip -6 neigh add proxy %s dev %s" % (ip, ifname), shell=True)


def proxy_del(ip, ifname):
    subprocess.call("ip -6 neigh del proxy %s dev %s" % (ip, ifname), shell=True)


def start():
    global size
    if not os.path.exists('/tmp/hosts/odhcpd'):
        return
    newsize = os.path.getsize('/tmp/hosts/odhcpd')
    if newsize == size:
        return
    else:
        size = newsize
    output = subprocess.check_output(['ip', 'neigh', 'show', 'proxy']).strip().split('\n')
    ndp_proxy = dict(zip([i.split(None, 1)[0] for i in output], [i.rsplit(None, 2)[-2] for i in output]))
    kernel_ip = set([i for i in ndp_proxy])
    f = open('/tmp/hosts/odhcpd', 'r')
    hosts_file_ip = set([line.split(None, 1)[0] for line in f if line[0] != '#'])
    f.close()
    for i in tuple(hosts_file_ip - kernel_ip):
        target = min(interfaces.items(), key=lambda x: x[1])[0]
        proxy_add(i, target)
        interfaces[target] += 1
    for i in tuple(kernel_ip - hosts_file_ip):
        target = ndp_proxy[i]
        proxy_del(i, target)
        interfaces[target] -= 1


while True:
    start()
    time.sleep(60)
